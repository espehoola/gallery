**REQUIREMENTS**
-
PHP7, PostgreSQL 9+

**INSTALL**
-
```
composer install
init
php yii migrate
```

**TESTING**
-
```
codecept run
```