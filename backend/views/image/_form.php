<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileinput\FileInput;
use common\models\Category;
use common\models\Image;

/* @var $this yii\web\View */
/* @var $model common\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(Category::getTitleArray()) ?>

    <?= $form->field($model, 'filename')->widget(FileInput::class, [
        'thumbnail' => empty($model->filename) ? '' : Html::img($model->getPathThumbs()),
        'style' => FileInput::STYLE_IMAGE,
        'options' => ['accept' => 'image/*']
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList(Image::getStatusArray()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
