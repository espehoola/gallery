<?php
/* @var $categories array */
?>

<?php if (!empty($categories)) { ?>
    <p>Category</p>

    <?php foreach ($categories as $category) { ?>
        <?php
        $active = false;
        if (strpos(Yii::$app->request->url, "[category_id]=" . $category['id'])) {
            $active = true;
        } ?>
        <a class="btn btn-default btn-block <?= !$active ?: ' btn-primary'?>" href="?Image[category_id]=<?= $category['id'] ?>">
            <?= $category['title'] ?>
        </a>
        <br>
    <?php } ?>
<?php } ?>
