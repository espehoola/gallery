<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $searchModel \common\models\Image */
/* @var $dataProvider \yii\data\ActiveDataProvider */
?>

<?php Pjax::begin(); ?>

<!-- Page Content -->
<div id="content">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list_item',
        'layout' => "{items}\n{pager}"
    ]);
    ?>
</div>

<?php Pjax::end() ?>
