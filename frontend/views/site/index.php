<?php

/* @var $this yii\web\View */
/* @var $searchModel \common\models\Image */
/* @var $dataProvider \yii\data\ActiveDataProvider */

?>
<div class="site-index">

    <?= $this->render('_list_view', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider])?>

</div>
