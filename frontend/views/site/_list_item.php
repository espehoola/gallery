<?php
/* @var $model \common\models\Image */
?>

<div class="img">
    <img class="img-responsive" src="/<?= $model->getPath() ?>" alt="<?= $model->title ?>">
</div>
<p class="text-primary">
    <?= $model->description ?>
</p>
<br>
<br>