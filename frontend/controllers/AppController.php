<?php

namespace frontend\controllers;


use common\models\Category;
use yii\web\Controller;


/**
 * Class AppController
 * @package frontend\controllers
 */
class AppController extends Controller
{
    /**
     * @var array
     */
    public $category;

    /**
     * Init AppController
     */
    public function init()
    {
        $this->category = Category::find()
            ->select(['id', 'title'])
            ->where(['status' => 1])
            ->asArray()
            ->all();
    }
}
