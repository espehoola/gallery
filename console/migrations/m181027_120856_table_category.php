<?php

use yii\db\Migration;

/**
 * Class m181027_120856_table_category
 */
class m181027_120856_table_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181027_120856_table_category cannot be reverted.\n";

        return false;
    }
}
