<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'uploadImg' => 'upload' . DIRECTORY_SEPARATOR . 'img',
    'uploadThumbs' => 'upload' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'thumbs',
];
