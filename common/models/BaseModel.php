<?php

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class BaseModel
 * @package common\models
 */
abstract class BaseModel extends ActiveRecord
{
    /**
     * @return array
     */
    public static function getTitleArray()
    {
        $query = static::find()->select(['id', 'title'])->where(['status' => 1])->asArray()->all();

        return ArrayHelper::map($query, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            0 => 'No active',
            1 => 'Active'
        ];
    }
}